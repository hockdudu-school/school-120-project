﻿using System.Collections.Generic;

namespace Timetables.Data.Repository
{
    public interface ILessonRepository
    {
        IEnumerable<Lesson> GetLessons();
        Lesson GetLessonById(int lessonId);
        Lesson GetNextLesson();
        void NewLesson(Lesson lesson);  
        void UpdateLesson(Lesson lesson);  
        void DeleteLesson(Lesson lesson);
    }
}