﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Timetables.Data.Repository
{
    public class LessonRepository : ILessonRepository
    {
        private readonly Context _context;

        public LessonRepository(Context context)
        {
            _context = context;
        }
        
        public IEnumerable<Lesson> GetLessons()
        {
            return _context.Lessons.ToList();
        }

        public Lesson GetLessonById(int lessonId)
        {
            return _context.Lessons.Find(lessonId);
        }

        public Lesson GetNextLesson()
        {
            var dateTimeNow = DateTime.Now;
            var now = new Time(dateTimeNow);
            var week = dateTimeNow.DayOfWeek;
            
            var currentLesson = (from lesson in _context.Lessons
                where lesson.DayOfWeek == week &&
                    (now.Hours > lesson.Start.Hours || now.Hours == lesson.Start.Hours && now.Minutes >= lesson.Start.Minutes) &&
                    (now.Hours < lesson.End.Hours || now.Hours == lesson.End.Hours && now.Minutes <= lesson.End.Minutes)
                select lesson)
                .OrderByDescending(lesson => lesson.Start.Hours).ThenByDescending(lesson => lesson.Start.Minutes)
                .ThenBy(lesson => lesson.End.Hours).ThenBy(lesson => lesson.End.Minutes)
                .FirstOrDefault();

            if (null != currentLesson)
            {
                return currentLesson;
            }

            var nextLesson = (from lesson in _context.Lessons
                where lesson.DayOfWeek == week &&
                      (lesson.Start.Hours > now.Hours || lesson.Start.Hours == now.Hours && lesson.Start.Minutes > now.Minutes) ||
                      lesson.DayOfWeek > week
                select lesson)
                .OrderBy(lesson => lesson.DayOfWeek)
                .ThenBy(lesson => lesson.Start.Hours).ThenBy(lesson => lesson.Start.Minutes)
                .ThenBy(lesson => lesson.End.Hours).ThenBy(lesson => lesson.End.Minutes)
                .FirstOrDefault();

            if (null != nextLesson)
            {
                return nextLesson;
            }

            var firstLesson = (from lesson in _context.Lessons
                where lesson.DayOfWeek < week
                select lesson)
                .OrderBy(lesson => lesson.DayOfWeek)
                .ThenBy(lesson => lesson.Start.Hours).ThenBy(lesson => lesson.Start.Minutes)
                .ThenBy(lesson => lesson.End.Hours).ThenBy(lesson => lesson.End.Minutes)
                .FirstOrDefault();

            return firstLesson;
        }

        public void NewLesson(Lesson lesson)
        {
            _context.Lessons.Add(lesson);
            Save();
        }

        public void UpdateLesson(Lesson lesson)
        {
            _context.Entry(lesson).State = EntityState.Modified;
            Save();
        }

        public void DeleteLesson(Lesson lesson)
        {
            _context.Lessons.Remove(lesson);
            Save();
        }

        private void Save()
        {
            _context.SaveChanges();
        }
    }
}