﻿using Timetables.Data.Repository;

namespace Timetables.Data
{
    public class RepositoryManager
    {
        private static readonly object SyncLock = new object();
        private static RepositoryManager _instance;

        private readonly LessonRepository _lessonRepository;
        
        private RepositoryManager()
        {
            var context = new Context();
            _lessonRepository = new LessonRepository(context);
        }

        public static ILessonRepository GetLessonRepository()
        {
            return GetInstance()._lessonRepository;
        }

        private static RepositoryManager GetInstance()
        {
            if (_instance == null)
            {
                lock (SyncLock)
                {
                    if (_instance == null)
                    {
                        _instance = new RepositoryManager();
                    }
                }
            }

            return _instance;
        }
    }
}