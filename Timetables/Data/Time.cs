﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Timetables.Data
{
    [ComplexType]
    public class Time : IComparable<Time>, IComparable
    {
        private int _hours;

        private int _minutes;

        public int Hours
        {
            get => _hours;
            set
            {
                if (value < 0 || value > 23)
                    throw new ArgumentException("Hours must be between 0 and 23");

                _hours = value;
            }
        }

        public int Minutes
        {
            get => _minutes;
            set
            {
                if (value < 0 || value > 59)
                    throw new ArgumentException("Minutes must be between 0 and 59");

                _minutes = value;
            }
        }

        public Time(DateTime dateTime)
        {
            Initialize(dateTime.Hour, dateTime.Minute);
        }

        public Time(int hours, int minutes)
        {
            Initialize(hours, minutes);
        }

        public Time()
        {
            Initialize(0, 0);
        }

        private void Initialize(int hours, int minutes)
        {
            Hours = hours;
            Minutes = minutes;
        }

        public override string ToString()
        {
            return $"{_hours:00}:{_minutes:00}";
        }

        public int CompareTo(object obj)
        {
            switch (obj)
            {
                case null:
                    return 1;
                case Time time:
                    return CompareTo(time);
                default:
                    throw new ArgumentException();
            }
        }

        public int CompareTo(Time other)
        {
            if (ReferenceEquals(this, other)) return 0;
            
            var hoursComparison = _hours.CompareTo(other._hours);
            return hoursComparison != 0 ? hoursComparison : _minutes.CompareTo(other._minutes);
        }
    }
}