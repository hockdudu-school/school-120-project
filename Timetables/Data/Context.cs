﻿using System.Data.Entity;

namespace Timetables.Data
{
    public class Context : DbContext
    {
        public Context() : base("name=TimetablesConnectionString")
        {
            this.Configuration.LazyLoadingEnabled = true;
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<Data.Context, Timetables.Migrations.Configuration>());
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
//            modelBuilder.Properties<DateTime>().Configure(c => c.HasColumnType("datetime2"));
        }
        public DbSet<Lesson> Lessons { get; set; }
    }
}
