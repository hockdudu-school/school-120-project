﻿using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;

namespace Timetables.Data
{
    public class Lesson
    {
        private Time _start = new Time();
        private Time _end = new Time();

        #region Database

        [Key] public long Id { get; set; }

        [Required]
        public Time Start
        {
            get => _start;
            set => _start = value ?? new Time();
        }

        [Required]
        public Time End
        {
            get => _end;
            set => _end = value ?? new Time();
        }

        public string Summary { get; set; }
        
        public string Teacher { get; set; }
        
        public string Location { get; set; }

        [Required]
        public DayOfWeek DayOfWeek { get; set; } = DayOfWeek.Monday;

        #endregion

        #region Applikationsschicht

        public override string ToString()
        {
            return $"{Summary}: {Start}-{End}";
        }

        #endregion
    }
}