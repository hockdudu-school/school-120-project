﻿using Timetables.Views;

namespace Timetables.States.States
{
    public class Empty : State
    {
        public Empty(ViewManager viewManager) : base(viewManager)
        {
        }

        public override State MarkDirty(StateManager stateManager)
        {
            return this;
        }
        
        public override State MarkClean(StateManager stateManager)
        {
            return this;
        }

        public override bool AttemptExitState()
        {
            return true;
        }

        public override string ToString()
        {
            return "Empty";
        }
    }
}