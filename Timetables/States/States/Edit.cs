﻿using Timetables.Views;

namespace Timetables.States.States
{
    public class Edit : State
    {
        public Edit(ViewManager viewManager) : base(viewManager)
        {
        }

        public override State MarkDirty(StateManager stateManager)
        {
            return stateManager.EditChanged;
        }
        
        public override State MarkClean(StateManager stateManager)
        {
            return this;
        }

        public override bool AttemptExitState()
        {
            return true;
        }

        public override string ToString()
        {
            return "Edit";
        }
    }
}