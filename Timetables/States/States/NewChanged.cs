﻿using System.Windows;
using Timetables.Views;

namespace Timetables.States.States
{
    public class NewChanged : State
    {
        public NewChanged(ViewManager viewManager) : base(viewManager)
        {
        }

        public override State MarkDirty(StateManager stateManager)
        {
            return this;
        }
        
        public override State MarkClean(StateManager stateManager)
        {
            return stateManager.New;;
        }

        public override bool AttemptExitState()
        {
            var messageBoxResult = MessageBox.Show(ViewManager.Window, FindResource("UnsavedNewText"),
                FindResource("UnsavedNew"), MessageBoxButton.OKCancel, MessageBoxImage.Exclamation);
            
            return messageBoxResult == MessageBoxResult.OK;
        }

        public override string ToString()
        {
            return "New (dirty)";
        }
    }
}