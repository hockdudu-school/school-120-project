﻿using Timetables.Views;

namespace Timetables.States.States
{
    public class New : State
    {
        public New(ViewManager viewManager) : base(viewManager)
        {
        }

        public override State MarkDirty(StateManager stateManager)
        {
            return stateManager.NewChanged;
        }
        
        public override State MarkClean(StateManager stateManager)
        {
            return this;
        }

        public override bool AttemptExitState()
        {
            return true;
        }

        public override string ToString()
        {
            return "New";
        }
    }
}