﻿using System.Windows;
using Timetables.Views;

namespace Timetables.States.States
{
    public class EditChanged : State
    {
        public EditChanged(ViewManager viewManager) : base(viewManager)
        {
        }

        public override State MarkDirty(StateManager stateManager)
        {
            return this;
        }
        
        public override State MarkClean(StateManager stateManager)
        {
            return stateManager.Edit;
            
        }

        public override bool AttemptExitState()
        {
            var messageBoxResult = MessageBox.Show(ViewManager.Window, FindResource("UnsavedChangesText"),
                FindResource("UnsavedChanges"), MessageBoxButton.OKCancel, MessageBoxImage.Exclamation);

            return (messageBoxResult == MessageBoxResult.OK);
        }

        public override string ToString()
        {
            return "Edit (dirty)";
        }
    }
}