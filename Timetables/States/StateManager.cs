﻿using Timetables.States.States;
using Timetables.Views;

namespace Timetables.States
{
    public class StateManager
    {
        public readonly State Empty;
        public readonly State View;
        public readonly State Current;
        public readonly State Edit;
        public readonly State EditChanged;
        public readonly State New;
        public readonly State NewChanged;

        public StateManager(ViewManager viewManager)
        {
            Empty = new Empty(viewManager);
            View = new View(viewManager);
            Current = new Current(viewManager);
            Edit = new Edit(viewManager);
            EditChanged = new EditChanged(viewManager);
            New = new New(viewManager);
            NewChanged = new NewChanged(viewManager);
        }
    }
}