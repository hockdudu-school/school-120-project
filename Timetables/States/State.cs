﻿using Timetables.Views;

namespace Timetables.States
{
    public abstract class State
    {
        protected readonly ViewManager ViewManager;

        protected State(ViewManager viewManager)
        {
            ViewManager = viewManager;
        }

        /// <summary>
        /// Marks the current state as dirty, if supported.
        /// </summary>
        /// <param name="stateManager">An instance of StateManager (for getting other states)</param>
        /// <returns>The new state after marking dirty</returns>
        public abstract State MarkDirty(StateManager stateManager);
        
        /// <summary>
        /// Marks the current state as clean, if supported.
        /// </summary>
        /// <param name="stateManager">An instance of StateManager (for getting other states)</param>
        /// <returns>The new state after marking clean</returns>
        public abstract State MarkClean(StateManager stateManager);

        /// <summary>
        /// Attempts to close the current state. If it's dirty, warn the user before closing.
        /// </summary>
        /// <returns>Whether the current state can be replaced by a new one or not.</returns>
        public abstract bool AttemptExitState();

        protected string FindResource(string key)
        {
            return (string) ViewManager.Window.FindResource(key);
        }
    }
}