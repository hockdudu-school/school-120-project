﻿using System;
using System.IO;
using System.Threading;
using System.Windows;

namespace Timetables
{
    public partial class App
    {
        private const string FallbackLanguage = "en";

        public App()
        {
            SetLanguageDictionary();   
        }
        
        private void SetLanguageDictionary()
        {
            var dictFallback = new ResourceDictionary {Source = GetUriForLanguage(FallbackLanguage)};
            Resources.MergedDictionaries.Add(dictFallback);

            var language = Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName;
            var dictLanguage = new ResourceDictionary();

            try
            {
                dictLanguage.Source = GetUriForLanguage(language);
                Resources.MergedDictionaries.Add(dictLanguage);
            }
            catch (IOException)
            {
                Logger.Log("App", $"Couldn't load language \"{language}\", using only fallback \"{FallbackLanguage}\"");
            }
        }

        private static Uri GetUriForLanguage(string language)
        {
            return new Uri($"i18n\\{language}.xaml", UriKind.Relative);
        }
    }
}
