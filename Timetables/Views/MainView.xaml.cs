﻿using System.Windows;
using Timetables.Data;
using Timetables.Views.UserControl;

namespace Timetables.Views
{
    public partial class MainWindow
    {
        private readonly ViewManager _viewManager;
        
        public MainWindow()
        {
            InitializeComponent();
            
            _viewManager = new ViewManager(GrLessonSingleViewWrapper, this);
            var lessonList = new LessonList(_viewManager);
            
            SetList(lessonList);
        }

        private void SetList(UIElement element) => GrLessonListWrapper.Children.Add(element);

        #region Handlers

        private void NewLesson_OnClick(object sender, RoutedEventArgs e)
        {
            _viewManager.ShowNewLesson();
        }

        private void CurrentLesson_OnClick(object sender, RoutedEventArgs e)
        {
            var lesson = RepositoryManager.GetLessonRepository().GetNextLesson();

            if (lesson != null)
            {
                _viewManager.ShowViewLesson(lesson, true);
            }
            else
            {
                Logger.Log("MainView", "LessonRepository.GetNextLesson() returned null");
            }
        }

        private void Exit_OnClick(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        #endregion

    }
}
