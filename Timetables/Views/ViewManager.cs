﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Timetables.Data;
using Timetables.States;
using Timetables.Views.UserControl.SingleView;

namespace Timetables.Views
{
    public delegate void LessonUpdatedHandler(Lesson lesson);

    public delegate void LessonCreatedHandler(Lesson lesson);

    public delegate void LessonDeletedHandler(Lesson lesson);

    public class ViewManager
    {
        private State State
        {
            get => _state;
            set
            {
                Logger.Log("ViewManager", $"Old State: {_state}; New State: {value}");
                _state = value;
            }
        }

        private Lesson CurrentLesson { get; set; }
        public Window Window { get; }

        public event LessonUpdatedHandler LessonUpdated;
        public event LessonCreatedHandler LessonCreated;
        public event LessonDeletedHandler LessonDeleted;
        
        private readonly Panel _wrapper;
        private readonly StateManager _stateManager;
        private State _state;
        
        public ViewManager(Panel wrapper, Window window)
        {
            _wrapper = wrapper;
            Window = window;
            
            _stateManager = new StateManager(this);

            InitializeView();
            AttachCloseListener();
        }

        private void InitializeView()
        {
            ShowEmptyLesson();
        }

        private void AttachCloseListener()
        {
            Window.Closing += Window_Closing;
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (!AttemptExitState())
            {
                e.Cancel = true;
            }
        }

        private void SetView(UIElement control)
        {
            _wrapper.Children.Clear();
            _wrapper.Children.Add(control);
        }
        
        private bool AttemptExitState()
        {
            return State == null || State.AttemptExitState();
        }

        public void ShowEmptyLesson()
        {
            if (!AttemptExitState()) return;
            
            CurrentLesson = null;
            State = _stateManager.Empty;
            
            SetView(new LessonEmpty());
        }

        public void ShowViewLesson(Lesson lesson, bool isCurrent = false)
        {
            if (!AttemptExitState()) return;
            
            CurrentLesson = lesson;
            State = isCurrent ? _stateManager.Current : _stateManager.View;

            var lessonView = new LessonView(lesson, this, isCurrent);
            
            SetView(lessonView);
        }

        public void ShowEditLesson(Lesson lesson)
        {
            if (!AttemptExitState()) return;
            
            CurrentLesson = lesson;
            State = _stateManager.Edit;
            
            var lessonEdit = new LessonEdit(lesson, this);
            SetView(lessonEdit);
        }

        public void ShowNewLesson()
        {
            if (!AttemptExitState()) return;
            
            var lesson = new Lesson();
            CurrentLesson = lesson;
            State = _stateManager.New;

            var lessonEdit = new LessonEdit(lesson, this, true);
            SetView(lessonEdit);
        }

        public void ShowDeleteLesson(Lesson lesson)
        {
            var result = MessageBox.Show(Window, "Do you really want to delete the lesson?", "Delete Lesson",
                MessageBoxButton.YesNo, MessageBoxImage.Exclamation);

            if (result == MessageBoxResult.Yes)
            {
                RepositoryManager.GetLessonRepository().DeleteLesson(lesson);
                TriggerLessonDeletion(lesson);
            }
        }

        public void MarkDirty()
        {
            State = State.MarkDirty(_stateManager);
        }

        public void MarkClean()
        {
            State = State.MarkClean(_stateManager);
        }

        public void TriggerLessonUpdate(Lesson lesson)
        {
            LessonUpdated?.Invoke(lesson);
        }
        
        public void TriggerLessonCreation(Lesson lesson)
        {
            LessonCreated?.Invoke(lesson);
        }

        public void TriggerLessonDeletion(Lesson lesson)
        {
            LessonDeleted?.Invoke(lesson);
        }
    }
}