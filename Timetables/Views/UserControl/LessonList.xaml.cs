﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Timetables.Data;

namespace Timetables.Views.UserControl
{
    public partial class LessonList
    {
        private CollectionView _view;

        private readonly ObservableCollection<Lesson> _lessons;

        private readonly ViewManager _viewManager;
        
        public LessonList(ViewManager viewManager)
        {
            InitializeComponent();
            
            _viewManager = viewManager;
            _viewManager.LessonUpdated += ViewManagerOnLessonUpdated;
            _viewManager.LessonCreated += ViewManagerOnLessonCreated;
            _viewManager.LessonDeleted += ViewManagerOnLessonDeleted;
            
            _lessons = new ObservableCollection<Lesson>(RepositoryManager.GetLessonRepository().GetLessons());

            PopulateList();
        }

        private void ViewManagerOnLessonUpdated(Lesson lesson)
        {
            _lessons.Remove(lesson);
            _lessons.Add(lesson);
        }
        
        private void ViewManagerOnLessonCreated(Lesson lesson)
        {
            _lessons.Add(lesson);
        }
        
        private void ViewManagerOnLessonDeleted(Lesson lesson)
        {
            _lessons.Remove(lesson);
        }

        private void PopulateList()
        {
            LvLessons.ItemsSource = _lessons;
            
            _view = (CollectionView) CollectionViewSource.GetDefaultView(LvLessons.ItemsSource);
            _view.GroupDescriptions.Add(new PropertyGroupDescription("DayOfWeek", new DayOfWeekConverter(this)));
            
            _view.SortDescriptions.Add(new SortDescription("DayOfWeek", ListSortDirection.Ascending));
            _view.SortDescriptions.Add(new SortDescription("Start", ListSortDirection.Ascending));
            _view.SortDescriptions.Add(new SortDescription("Summary", ListSortDirection.Ascending));
        }
        
        private Lesson GetSelectedLesson()
        {
            return GetSelectedLessonFromListViewIndex(LvLessons.SelectedIndex);
        }

        private Lesson GetSelectedLessonFromListViewIndex(int listViewIndex)
        {
            return listViewIndex != -1 ? _view.GetItemAt(listViewIndex) as Lesson : null;
        }

        private void MouseClick(object sender, MouseButtonEventArgs e)
        {
            var lesson = GetSelectedLesson();

            if (lesson != null)
            {
                _viewManager.ShowViewLesson(lesson);
            }
        }
        
        private void ContextMenuEdit(object sender, RoutedEventArgs e)
        {
            var lesson = GetSelectedLesson();
            
            if (lesson != null)
            {
                _viewManager.ShowEditLesson(lesson);                
            }
        }

        private void ContextMenuDelete(object sender, RoutedEventArgs e)
        {
            var lesson = GetSelectedLesson();
            
            if (lesson != null)
            {
                _viewManager.ShowDeleteLesson(lesson);
            }
        }
    }

    [ValueConversion(typeof(DayOfWeek), typeof(string))]
    internal class DayOfWeekConverter : IValueConverter
    {
        private readonly FrameworkElement _frameworkElement;

        internal DayOfWeekConverter(FrameworkElement frameworkElement)
        {
            _frameworkElement = frameworkElement;
        }
        
        
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Debug.Assert(value != null);
            Debug.Assert(value.GetType() == typeof(DayOfWeek));
            
            var from = (DayOfWeek) value;

            return (string) _frameworkElement.FindResource(from.ToString());
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}