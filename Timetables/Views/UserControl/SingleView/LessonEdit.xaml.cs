﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Timetables.Data;

namespace Timetables.Views.UserControl.SingleView
{
    /// <summary>
    /// Interaction logic for EditLesson.xaml
    /// </summary>
    public partial class LessonEdit
    {
        private static readonly DayOfWeek[] DayOfWeeks =
        {
            DayOfWeek.Monday,
            DayOfWeek.Tuesday,
            DayOfWeek.Wednesday,
            DayOfWeek.Thursday,
            DayOfWeek.Friday,
            DayOfWeek.Saturday,
            DayOfWeek.Sunday,
        };

        public Lesson Lesson { get; }

        private readonly ViewManager _viewManager;
        private readonly bool _isNew;
        private bool _wasAlreadyMarkedDirty;

        public LessonEdit(Lesson lesson, ViewManager viewManager, bool isNew = false)
        {
            InitializeComponent();

            Lesson = lesson;
            _viewManager = viewManager;
            _isNew = isNew;

            InitializeDaysOfWeek();
            UpdateForm();
            AddChangeListener();
        }

        private void InitializeDaysOfWeek()
        {
            CbDayOfWeek.ItemsSource = DayOfWeeks.Select(dow => FindResource(dow.ToString()));
        }

        private void UpdateForm()
        {
            CbDayOfWeek.SelectedIndex = Array.IndexOf((Array) DayOfWeeks, Lesson.DayOfWeek);
            TpStart.Value = new DateTime(1970, 1, 1, Lesson.Start.Hours, Lesson.Start.Minutes, 0);
            TpEnd.Value = new DateTime(1970, 1, 1, Lesson.End.Hours, Lesson.End.Minutes, 0);
            TbTeacher.Text = Lesson.Teacher;
            TbLocation.Text = Lesson.Location;
            TbModule.Text = Lesson.Summary;
        }

        private void AddChangeListener()
        {
            CbDayOfWeek.SelectionChanged += (sender, args) => MarkUpdated();
            TpStart.ValueChanged += (sender, args) => MarkUpdated();
            TpEnd.ValueChanged += (sender, args) => MarkUpdated();
            TbTeacher.TextChanged += (sender, args) => MarkUpdated();
            TbLocation.TextChanged += (sender, args) => MarkUpdated();
            TbModule.TextChanged += (sender, args) => MarkUpdated();

            CbDayOfWeek.LostFocus += (sender, args) => Validate(ValidationIdentifier.DayOfWeek);
            TpStart.LostFocus += (sender, args) => Validate(ValidationIdentifier.StartTime);
            TpEnd.LostFocus += (sender, args) => Validate(ValidationIdentifier.EndTime);
        }

        private void MarkUpdated()
        {
            BtSave.IsEnabled = Validate(skipLabel: true);
            
            if (_wasAlreadyMarkedDirty) return;
            _viewManager.MarkDirty();
            _wasAlreadyMarkedDirty = true;
        }

        private void EditLesson_Submit(object sender, RoutedEventArgs e)
        {
            if (!Validate())
            {
                return;
            }

            Lesson.Teacher = TbTeacher.Text;
            Lesson.Summary = TbModule.Text;
            Lesson.Location = TbLocation.Text;
            
            // If it were null, then Validate() would've failed already
            Debug.Assert(null != TpStart.Value);
            Debug.Assert(null != TpEnd.Value);
            
            var dateTimeStart = (DateTime) TpStart.Value;
            var dateTimeEnd = (DateTime) TpEnd.Value;

            var timeStart = new Time(dateTimeStart.Hour, dateTimeStart.Minute);
            var timeEnd = new Time(dateTimeEnd.Hour, dateTimeEnd.Minute);

            Lesson.Start = timeStart;
            Lesson.End = timeEnd;

            var dayOfWeekSelectedIndex = CbDayOfWeek.SelectedIndex;
            Debug.Assert(0 <= dayOfWeekSelectedIndex && dayOfWeekSelectedIndex < DayOfWeeks.Length);
            Lesson.DayOfWeek = DayOfWeeks[dayOfWeekSelectedIndex];

            if (_isNew)
            {
                RepositoryManager.GetLessonRepository().NewLesson(Lesson);
                _viewManager.TriggerLessonCreation(Lesson);
            }
            else
            {
                RepositoryManager.GetLessonRepository().UpdateLesson(Lesson);
                _viewManager.TriggerLessonUpdate(Lesson);
            }

            _viewManager.MarkClean();
            _viewManager.ShowViewLesson(Lesson);
        }

        private void EditLesson_Cancel(object sender, RoutedEventArgs e)
        {
            if (_isNew) {
                _viewManager.ShowEmptyLesson();
            }
            else
            {
                _viewManager.ShowViewLesson(Lesson);
            }
        }

        private bool Validate(ValidationIdentifier validationIdentifier = ValidationIdentifier.All, bool skipLabel = false)
        {
            switch (validationIdentifier)
            {
                case ValidationIdentifier.DayOfWeek:
                    return ValidateDayOfWeek(skipLabel);
                case ValidationIdentifier.StartTime:
                    return ValidateStartTime(skipLabel);
                case ValidationIdentifier.EndTime:
                    return ValidateEndTime(skipLabel);
                case ValidationIdentifier.None:
                    return true;
                case ValidationIdentifier.All:
                    return ValidateDayOfWeek(skipLabel) && ValidateStartTime(skipLabel) && ValidateEndTime(skipLabel);
                default:
                    throw new ArgumentOutOfRangeException(nameof(validationIdentifier), validationIdentifier, null);
            }
        }

        private bool ValidateStartTime(bool skipLabel = false)
        {
            var isStartTimeValid = TpStart.Value != null;

            if (!skipLabel)
            {
                LbStartError.Visibility = isStartTimeValid ? Visibility.Collapsed : Visibility.Visible;
            }

            return isStartTimeValid;
        }

        private bool ValidateEndTime(bool skipLabel = false)
        {
            var isEndTimeValid = TpEnd.Value != null;

            if (!skipLabel)
            {
                LbEndError.Visibility = isEndTimeValid ? Visibility.Collapsed : Visibility.Visible;
            }

            return isEndTimeValid;
        }

        private bool ValidateDayOfWeek(bool skipLabel = false)
        {
            var isDayOfWeekValid = CbDayOfWeek.SelectedIndex != -1;

            if (!skipLabel)
            {
                LbDayOfWeekError.Visibility = isDayOfWeekValid ? Visibility.Collapsed : Visibility.Visible;
            }
            
            return isDayOfWeekValid;
        }

        private enum ValidationIdentifier
        {
            DayOfWeek,
            StartTime,
            EndTime,
            None,
            All
        }
    }
}