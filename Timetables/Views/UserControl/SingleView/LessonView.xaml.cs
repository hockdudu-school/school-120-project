﻿using System;
using System.Timers;
using System.Windows;
using Timetables.Data;

namespace Timetables.Views.UserControl.SingleView
{
    public partial class LessonView
    {
        private Timer _timer;
        private readonly ViewManager _viewManager;
        private readonly Lesson _lesson;
        private readonly bool _isCurrent;
        
        public LessonView(Lesson lesson, ViewManager viewManager, bool isCurrent = false)
        {
            InitializeComponent();

            _viewManager = viewManager;
            _lesson = lesson;
            _isCurrent = isCurrent;
            
            Unloaded += OnUnloaded;
            
            DisplayLesson();
        }

        private void DisplayLesson()
        {
            TbLocation.Text = _lesson.Location;
            TbStart.Text = _lesson.Start.ToString();
            TbEnd.Text = _lesson.End.ToString();
            TbDayOfWeek.Text = (string) FindResource(_lesson.DayOfWeek.ToString());
            TbTeacher.Text = _lesson.Teacher;
            TbSummary.Text = _lesson.Summary;

            if (_isCurrent)
            {
                BindProgressBar();
            }
        }

        private void BindProgressBar()
        {
            var periodTimeSpan = TimeSpan.FromMinutes(1);

            _timer = new Timer {Interval = periodTimeSpan.TotalMilliseconds, AutoReset = true};
            _timer.Elapsed += (sender, args) => DoActivateProgressBar();
            _timer.Start();
            
            DoActivateProgressBar();
        }
        
        private void DoActivateProgressBar() {
            if (!IsLessonRightNow(_lesson))
            {
                var newCurrentLesson = RepositoryManager.GetLessonRepository().GetNextLesson();
                    
                if (null != newCurrentLesson && newCurrentLesson.Id != _lesson.Id)
                {
                    _viewManager.ShowViewLesson(newCurrentLesson, true);    
                }
                
                PbProgress.Visibility = Visibility.Collapsed;
                return;
            }

            PbProgress.Visibility = Visibility.Visible;

            PbProgress.Minimum = 0;
            PbProgress.Maximum = 1;
            PbProgress.Value = GetLessonProgress(_lesson);
        }

        private static bool IsLessonRightNow(Lesson lesson)
        {
            var now = new Time(DateTime.Now);

            return lesson.Start.CompareTo(now) <= 0 && lesson.End.CompareTo(now) >= 0;
        }

        private static double GetLessonProgress(Lesson lesson)
        {
            if (!IsLessonRightNow(lesson)) return 0d;
            
            var now = new Time(DateTime.Now);

            var startMinutes = GetMinutesFromTime(lesson.Start);
            var endMinutes = GetMinutesFromTime(lesson.End);
            var nowMinutes = GetMinutesFromTime(now);

            var minutesTotal = endMinutes - startMinutes;
            var nowOffset = nowMinutes - startMinutes;

            return nowOffset / (double) minutesTotal;
        }

        private static int GetMinutesFromTime(Time time)
        {
            return time.Hours * 60 + time.Minutes;
        }

        private void OnUnloaded(object sender, RoutedEventArgs e)
        {
            _timer?.Stop();
        }

        private void BtEdit_OnClick(object sender, RoutedEventArgs e)
        {
            _viewManager.ShowEditLesson(_lesson);
        }
    }
}