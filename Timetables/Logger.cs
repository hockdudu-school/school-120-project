﻿using System;
using System.Diagnostics;

namespace Timetables
{
    public class Logger
    {
        public static void Log(string owner, string message)
        {
            var time = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
            Debug.Print($"[{time}] {owner}: {message}");
        }
    }
}