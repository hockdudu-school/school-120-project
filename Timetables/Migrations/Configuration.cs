using System.Data.Entity.Migrations;

namespace Timetables.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<Timetables.Data.Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "Timetables.Data.Context";
        }

        protected override void Seed(Timetables.Data.Context context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
